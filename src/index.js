/* global process */
'use strict'

/**
 * Dependencies
 * @ignore
 */
const cwd = process.cwd()
const path = require('path')
const express = require('express')
const morgan = require('morgan')

/**
 * Dotenv
 * @ignore
 */
require('dotenv').config()

/**
 * Express 
 * @ignore
 */
const { 
    PORT: port = 3000 
} = process.env
const app = express()

app.use(morgan('tiny'))
app.use(express.static('public'))

// GET /message
app.get('/message', (req, res) => {
    res.json({
        message: 'Hello, world!'
    })
})

/**
 * Launch
 * @ignore
 */
app.listen(port, () => console.log(`Listening on port ${port}.`))
